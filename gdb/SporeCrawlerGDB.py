import gdb
import os
import argparse
import angr
import claripy
import time
import re
import logging
import subprocess

ARG_COUNT=5
ARG_SIZE=10
TARGET="rip#"

class SporeCrawlerGDB(gdb.Command):
	def __init__(self):
		super().__init__("scrawl",gdb.COMMAND_DATA)
		self.args = None
		self.parser = None
		self.stdin = 16
		self.is_env = False
		self.filename = ""
		self.base_address = 0
		self.target = 0
		self.sym_env = None
		self.env_dict = {}		

		self.usage = "[Usage]: scrawl -h for help"
		
		## not implemented ##
		#parser.add_argument("-S","--strings",help="returns only strings found during taint analysis including\
		#					 static strings and those included in function arguments",action='store_true',default=False)	
		
		## not implemented ##
		#parser.add_argument("--string_arg_length",help="byte length of strings to print out",default=16)	


		## not implemented ##
		#parser.add_argument("-u","--unique",help="try to only display unique function calls discovered",default=False)	
	
	def _check_args(self,args):

		self.args = self.parser.parse_args(args)
		print(self.args)	
		if (self.args != None):
			self.stdin  = int(self.args.stdin)
			self.arg_count = int(self.args.arg_count)
			self.arg_length = int(self.args.arg_length)
			self.step_count = int(self.args.step_count)

			if (self.args.binary):
				self.filename = self.args.binary
			else:
				self.filename = self.get_binary_filename(False)
			if (self.args.env):
				if (self.args.env != "all"):
					self.sym_env = self.env_dict
					names = [self.args.env]

				else:
					names = self.args.env.split(" ")	
				for name in names:
					try:
						literal = self.env_dict[name]
						self.sym_env[name] = claripy.BVS("%s" % (name),len(literal)*0x8)
					except KeyError:
						self.sym_env.update({name: claripy.BVS("%s" % (name),16*0x8)})
				
		return False

	def get_env(self,tty):
		out = gdb.execute("show environment",from_tty=tty,to_string=True)
		for var_line in out.split("\n"):
			_var = var_line.split("=")
			if (len(_var) == 2):
				self.env_dict[_var[0]]=_var[1]
		
	def get_binary_filename(self,tty):
		out = gdb.execute("info files",from_tty=False,to_string=True)
		top_line = out.split("\n")[0]
		filename = top_line.split("\"")[1]
		return filename

	def get_reg_value(self, reg, tty):

		out = ""
		value = ""
		if self.is_reg(reg):
			out = gdb.execute("print /x $%s" % (reg),from_tty=False,to_string=True)
			value = int(out.split("=")[1].replace(' ','').replace('\n',''),16);	
		return value

	def get_base_address(self,tty):
		out = ""
		value = ""
		try:
			out = gdb.execute("info inferior",from_tty=False,to_string=True) #assuming 1 inferior
			pid = out.split("\n")[1].split(" ")[6]
			#print("[*] pid :=> %s" % (pid))	
			output = subprocess.check_output(["/usr/bin/pmap","-x", "%s" % (pid)])
			base = output.decode("utf-8").split("\n")[2].split(" ")[0].replace(' ','')
			return int(base,16)

		except:
			return -1

	def is_reg(self,reg):
		return reg in REGNAMES

	def taint_search(self):
	

		self.get_target()
		self.project = angr.Project(self.filename,
					load_options={"auto_load_libs":False,
							"main_opts":{"base_addr":self.base_address,
									"force_rebase":True
							}})

		self.sym_args = [claripy.BVS("arg_%d" % (i),self.arg_length*0x8) for i in range(self.arg_count)]
		self.sym_stdin = claripy.BVS("stdin",0x8*self.stdin)
		logging.getLogger('angr').setLevel('CRITICAL')
		entry_state = self.project.factory.full_init_state(args=[self.filename]+self.sym_args)
		
		print("base address : [%s]" % (hex(self.base_address)))

		if self.args and self.args.env:
			entry_state = self.project.factory.full_init_state(args=self.sym_args,stdin=self.sym_stdin,env=self.sym_env)
		else:
			entry_state = self.project.factory.full_init_state(args=self.sym_args,stdin=self.sym_stdin)
		#try:
		self.simmgr = self.project.factory.simulation_manager(entry_state)
		run = self.project.analyses.SporeCrawlerAnalysis(filename=self.filename,
								initial_state=entry_state,
								steps=self.step_count,
								use_dm=args.directed_exec,
								limit=args.limit)
		#except Exception as e:
		#	exc_type, exc_obj,exc_tb = sys.exc_info()
		#	fname  = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		#	print(exc_type,fname,exc_tb.tb_lineno)
		#	print(e)
		#	print(self.usage)


	def get_target(self,tty=None):
		self.base_address = self.get_base_address(tty)
		if self.base_address == -1:
			raise Exception("couldn't get base address")
		if self.args == None or (self.args.binary != None and  len(self.args.binary)) == 0:
			self.filename = self.get_binary_filename(tty)
		return self.filename
		
	def invoke(self, args,tty):
		_args = None
		#try:
		self.parser = argparse.ArgumentParser()
		self.parser.add_argument("-D","--directed_exec",
					help="Use directed symbolic execution",
					action='store_true',
					default=False)	

		self.parser.add_argument("-e","--env",
					help="symbolize env",
					type=str,
					default="HOME")	

		self.parser.add_argument("-b","--binary",
					help="Binary to parse",
					type=str)	

		self.parser.add_argument("-c",
					"--arg_count",
					help="Number of arguments",
					default=3,
					type=int)	

		self.parser.add_argument("-l",
					"--arg_length",
					help="Bit length of each argument",
					default=16,
					type=int)	

		self.parser.add_argument("-L",
					"--limit",
					help="maximum number of states to keep alive during depth first search",
					type=int,
					default=1)	

		self.parser.add_argument("-s",
					"--step_count",
					help="Number of symbolic execution steps",
					default=256,
					type=int)	

		self.parser.add_argument("--stdin",
					help="symbolize stdin, argument is the length of stdin as a byte-wise claripy.BVS",
					default=64,
					type=type(1))	

		self.get_env(tty)	
		print(args.split(" "))
		self._check_args(args.split(" "))
		self.taint_search()

		#except Exception as e:
		#	exc_type, exc_obj,exc_tb = sys.exc_info()
		#	fname  = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		#	print(exc_type,fname,exc_tb.tb_lineno)
		#	print(e)
		#	print(self.usage)
SporeCrawlerGDB()
