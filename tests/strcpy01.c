#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char **argv){
	char *dest = malloc(sizeof(char)*10);
	if (!dest){
		return -1;
	}
	if (strcmp(argv[1],"-strcpy") == 0){
		strcpy(dest,argv[1]);
		printf("dest: %s\n", dest);
		free(dest);

		return 0;
	}
	else if (strcmp(argv[1],"-strncpy") == 0){

		strncpy(dest,argv[1],strlen(argv[1]));
		printf("dest: %s\n", dest);
		free(dest);
		return 0;
	}
	else if (strcmp(argv[1],"-memcpy") == 0){

		memcpy(dest,argv[1],strlen(argv[1]));
		printf("dest: %s\n", dest);
		free(dest);
		return 0;
	}
	else if (strcmp(argv[1],"-memset") == 0){

		memset(dest,0x0,sizeof(char)*strlen(argv[1]));
		free(dest);
		return 0;
	}
	else if (strcmp(argv[1],"-fake1") == 0){
		//fake option to make sure we aren't picking up false positives
		strcpy(dest,"fake");
		free(dest);
		return 0;
	}
	else if (strcmp(argv[1],"-fake2") == 0){
		//fake option to make sure we aren't picking up false positives
		strcpy(dest,"fake");
		free(dest);
		return 0;
	}
	else if (strcmp(argv[1],"-fake3") == 0){
		//fake option to make sure we aren't picking up false positives
		strcpy(dest,"fake");
		free(dest);
		return 0;
	}
	else{
		printf("[x] wrong option\n");
	}
	return -1;
}
