#!/usr/bin/python3

import argparse
import angr
import claripy

from . import SporeCrawlerAnalysis
if __name__ == "__main__":
	
	parser = argparse.ArgumentParser()
	
	parser.add_argument("-D","--directed_exec",help="Use directed symbolic execution",action='store_true',default=False)	

	parser.add_argument("-b","--binary",help="Binary to parse")	
	parser.add_argument("-c","--arg_count",help="Number of arguments",default=3)	
	parser.add_argument("-l","--arg_length",help="Bit length of each argument",default=16)	
	parser.add_argument("-L","--limit",help="maximum number of states to keep alive during depth first search",type=int,default=1)	
	parser.add_argument("-s","--step_count",help="Number of symbolic execution steps",default=256)	
	parser.add_argument("--stdin",help="symbolize stdin, argument is the length of stdin as a byte-wise claripy.BVS",default=64,type=type(1))	
	args = parser.parse_args()

	binary_name = args.binary
	arg_count = int(args.arg_count)
	arg_length = int(args.arg_length)
	step_count = int(args.step_count)

	project = angr.Project(binary_name,load_options={"auto_load_libs":False,"main_opts":{"base_addr":0x400000, "force_rebase":False}});
	
	print("|--binary :=> %s" % (binary_name))
	print("|--arg_count :=> %s" % (arg_count))
	print("|--arg_length :=> %s" % (arg_length))

	_args = [claripy.BVS("cmd_arg%s" % (i), 8*arg_length,annotations=(claripy.Annotation(),)) for i in range(arg_count)]
	_sym_stdin = claripy.BVS("sym_stdin", 8*args.stdin,annotations=(claripy.Annotation(),))
	entry_state = project.factory.entry_state(args=_args,stdin=_sym_stdin)
	global_address_access = project.analyses.SporeCrawlerAnalysis(filename=binary_name,
									initial_state=entry_state,
									project=project,
									steps=step_count,
									use_dm=args.directed_exec,
									limit=args.limit)
