# SporeCrawler [work in progress]


SporeCrawler is a front end to Angr that exposes some nifty taint analysis and symbolic execution based functionality. SporeCrawler accepts
ELF x86/64 binaries as input and reports on tainted function calls (with an easy to extend plugin framework). Spore Crawler is also a 
gdb plugin that allows the same functionality except accessible during a live gdb session. These scripts are for anyone that wants
to make use of symbolic execution but doesn't want to write a number of buggy scripts everytime, SporeCrawler
at first invocation gets the job done for most problems are requires only the binary you're targeting. 

Who would like this?
* Folks who need to know what input parameters will reach tainted memcpy's, strcpys (and other configurable functions)
* Folks who want to get data on the strings being thrown around in a binary (basically a 3d "strings", because it see's inside functions too)
* Folks who want to automate the process of finding out what the result of snippets of execution will be, without running the entire binary

# What is Taint Analysis and Symbolic Execution?

**Taint Analysis** is essentially the process of determining whether an input to a program block is affected by untrusted or select input parameters.
Approaches to taint analysis are split into two categories (as with much of static analysis): 1) static taint analysis which makes use of only 
code or definitions and does not concretely execute code at all; and the other approach 2) dynamic taint analysis, making use of live and concrete runs in a staged
 environment, or via an instrumented binary. Each has its own drawbacks for instance static analysis may over-approximate behaviour in code and report
false postives, although dynamic analysis may underapproximate and miss cases due its reliance on execute path ways. When a property of a program 
block is dependent on a tainted value we tend to refer to it as a tainted program block, bare in mind this means either a jmp or register has been
affected by an input paramter in some way. For more details on how taint analysis works in SporeCrawler see the "Further Reading and References" section and the source 
code.

**Symbolic Execution** is also a very powerful static analysis method again with a sister technique that mixes in concrete execution called "concolic"
 execution. Symbolic execution reasons about input programs have by reliying on a constraint solver to determine which states are reachable under a 
given input. The method makes for incredibly sophisticated fuzzing, malware analysis and in general any formal program analysis.

These two powerhouses of program analysis are combined in SporeCrawler in order to find dangerous functions with tainted program blocks-- meaning
 something in the program block, a register or memory location or jmp instruction, was affected by our input.
 
# Install

SporeCrawler is pretty easy to install, it just requires a few things to be in place.

## 1. Install Angr

SporeCrawler requires Angr to be available with all its bells and whistles. since it is just a simple frontend. In order
to get the deets on how to install angr, check out their guide here: [https://github.com/angr/angr]

## 2. Download SporeCrawler

Getting spore crawler is pretty straight forward, just clone this repo.
```
git clone git@gitlab.com:k3170makan/sporecrawler.git
```

## 3. GDB config for SporeCrawler

In order to make sure SporeCrawler is available to gdb when it starts up, try adding this to your ```~/.gdbinit``` file

```
gdb
gdb> source  [path to sporecrawler repo]/gdb/gdb_sporecrawler.py

```

## 4. Test run

Quick example run to make sure you're ready to rock, here we run SporeCrawler on gnuplot, since it has nice behaviour:
```
./SporeCrawler.py -b /usr/bin/gnuplot
```
or you may want to use the test script i included:
```
cd ./tests/
./sporecrawler_test.sh [function_name]

```
Where ```[function_name]``` is the name of a function you'd like to search for in the binaries (they will naturally include others that trigger reports too)
here's a sample run:
```
>$ ./sporecrawler_test.sh strcpy
|--binary :=> /usr/bin/dbus-cleanup-sockets
|--arg_count :=> 10
|--arg_length :=> 16
[*] CFG derivation successfull!

|--[<SimState @ 0x500028>]> strncpy (dest=['0x7fffffffffefdc2'], src=[<BV256 symbolic_read_unconstra...],  n=[107])
|--[<SimState @ 0x500030>]> strcpy (dest=[<SAO <BV64 unconstrained_ret_s], src=[<BV64 (if BoolS(readdir_cond_4...])
[/usr/bin/dbus-cleanup-sockets>] {14} (1) <SimState @ 0x401351> -0-

```
The test script basically finds all the binaries under ```/usr/bin/``` looks for any that have strings mentioning ```[function_name]``` and then returns a random one from this list and runs sporecrawler on it.

# Usage

There are a couple things one can achieve with sporecrawler but they do require having some background in symbolic execution to have a real understanding,
in order get folks going who don't need all that background now, I've devised the following examples, these should give you enough understanding to solve
problems with SporeCrawler.

## Taint Analysis with SporeCrawler

When you give spore crawler a binary it will automatically look for dangerous C functions (strcpy, memcpy, strncpy, etc) that are tainted. Here's what a typical invokation looks like:
```
$./main.py -b /usr/bin/openjade-1.4devel -L 3 -D

|--binary :=> /usr/bin/openjade-1.4devel
|--arg_count :=> 3
|--arg_length :=> 16
[*] CFG derivation successfull!
|--[<SimState @ 0x500d40>]> memmove (dest=['0x0'], src=[<BV256 Reverse(mem_0_635_64{UN...], n=[0])
|--[<SimState @ 0x500d40>]> memmove (dest=[<SAO <BV64 mem_7fffffffffefa00], src=[<BV64 mem_7fffffffffefa00_731_...], n=[<BV64 (mem_7fffffffffef9f8_735])
|--[<SimState @ 0x500d40>]> memmove (dest=[<SAO <BV64 mem_7fffffffffef9a0], src=[<BV64 mem_7fffffffffef9a0_747_...], n=[<BV64 (mem_7fffffffffef998_749])
|--[<SimState @ 0x500c38>]> memcpy (dest=[0], src=[b' \x00}{\\fldrslt\x00E_\x00\\...], n=[1])
		|--- arg_0 :=> b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 
		|--- arg_1 :=> b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 
		|--- arg_2 :=> b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 

|--[<SimState @ 0x500c38>]> memcpy (dest=[<BV64 unconstrained_ret___cxa_], src=[b'Default Tbl Format\x00Defaul...], n=[18])
|--[<SimState @ 0x500c38>]> memcpy (dest=[<BV64 unconstrained_ret___cxa_], src=[b'Default Pgf Format\x00Page N...], n=[18])
|--[<SimState @ 0x500c38>]> memcpy (dest=[<BV64 unconstrained_ret___cxa_], src=[b'Times New Roman\x0095\x00Sym...], n=[15])
|--[<SimState @ 0x500c38>]> memcpy (dest=[<BV64 unconstrained_ret___cxa_], src=[b'Page Number Format\x00A\x00R...], n=[18])
|--[<SimState @ 0x500c38>]> memcpy (dest=[<BV64 unconstrained_ret___cxa_], src=[b'A\x00Regular\x00Bold\x00Ital...], n=[1])
[/usr/bin/openjade-1.4devel>] {8} (1) <SimState @ 0x422700> -0-
```

As you can see, SporeCrawler starts reporting the memcpy's and strcpy's it finds and where possible it displays some of the string literals involved.
The prompt ```[/usr/bin/openjade-1.4devel>] {8} (1) <SimState @ 0x422700> -0-``` displays some useful information about your symbolic execution run:
* ```[/usr/bin/openjade-1.4devel>]``` shows the binary you're currently working on
* ```{8}``` shows the number of symbolic execution steps that have passed
* ```(1)``` shows the number of active states under-going execution, one can adjust this with a parameter to increase discovery yeild
* ```<SimState @ 0x422700>``` shows the current top of the active state list, as a sample to show how quickly new states are discovere
* ```-0-``` shows the number of deadended states, this usually indicates that execution is not stuck and helps detect loops


## Caveats and Gotchas

Some caveats to remember about how spore crawler works and what kind of taint analysis its doing and the drawbacks it suffers due to this. 
The taint analysis method works by tagging symbolized input arguments with annotations and setting hooks for popular functions, as soon as a 
function is symbolically executed it is checked for a tag/taint. 

This means that:
* SporeCrawler doesn't actually run code, at the moment---pure static analysis---it simply uses the claripy ASTs and their annotation propogation to check 
if something is tainted. What that means is that it may likely over-approximate some things it finds and currently it may report tainted calls inside libraries 
or dynamically loaded libraries---which is good and complete but in a real life situation often doesn't result in bugs becuase external libraries tend to be pretty well scrubbed. 
* Also the output is reported as taints come it during a live symbolic execution of the code---not concrete execution! Due to this, reports of memcpy's or functions 
found tainted will by default be reported redudantly, I am making provision so reports remain unique at the moment.
* When a state is tainted due to the way claripy's ASTs propogates them it seems inaccurate in reporting precisely what part of the state's registers or memory is actually
tainted by input. So please not we know that something is definitely tainted, its not so clear what part of the state actually carries in the taint---also working on this!
 

### Discovering Commandline Options and Strings

Because of how SporeCrawler works it is actuall aware of the contents of strcpy, memcpy and other functions calls, parameters! This
means you can often find commandline options in a binary, and confirm that they are influenced by input, see the following example from
the test cases:

```
>$ ./main.py -b tests/strcpy01.elf -D -s 1024
|--binary :=> tests/strcpy01.elf
|--arg_count :=> 3
|--arg_length :=> 16
[*] CFG derivation successfull!
|--[<SimState @ 0x500018>]> strcpy (dest=['0xc0000f20'], src=[b'-strcpy\x00\x80\x80\x80\x80\...])
		|--- arg_0 :=> b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 
		|--- arg_1 :=> b'-strcpy\x00\x80\x80\x80\x80\x80@\x80\x80' 
		|--- arg_2 :=> b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 
|--[<SimState @ 0x500010>]> strncpy (dest=['0xc0000f20'], src=[b'-strncpy\x00\x80\x80\x00\x00...],  n=[8])
		|--- arg_0 :=> b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 
		|--- arg_1 :=> b'-strncpy\x00\x80\x80\x00\x00\x00\x00\x00' 
		|--- arg_2 :=> b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 
|--[<SimState @ 0x500048>]> memcpy (dest=[3221229344], src=[b'-memcpy\x00\x80\x80\x80\x80\...], n=[7])
		|--- arg_0 :=> b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 
		|--- arg_1 :=> b'-memcpy\x00\x80\x80\x80\x80\x80\x80\x80@' 
		|--- arg_2 :=> b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 
|--[<SimState @ 0x500038>]> memset (s=[b'\x00\x00\x00\x00\x00\x00\x00], c=[<SAO <BV64 0x0>>], n=[7])
		|--- arg_0 :=> b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 
		|--- arg_1 :=> b'-memset\x00\x80\x80\x80\x80\x80\x80\x00\x00' 
		|--- arg_2 :=> b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 
```
As we can see in the above example, options like ```-strncpy```, ```-memcpy``` are discovered this is since
looking at the binary, they are actual paths to tainted strcpy and memcpy calls:

```
>$ cat tests/strcpy01.c 
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char **argv){
	char *dest = malloc(sizeof(char)*10);
	if (!dest){
		return -1;
	}
	if (strcmp(argv[1],"-strcpy") == 0){
		strcpy(dest,argv[1]);
		printf("dest: %s\n", dest);
		free(dest);

		return 0;
	}
	else if (strcmp(argv[1],"-strncpy") == 0){

		strncpy(dest,argv[1],strlen(argv[1]));
		printf("dest: %s\n", dest);
		free(dest);
		return 0;
	}
	else if (strcmp(argv[1],"-memcpy") == 0){

		memcpy(dest,argv[1],strlen(argv[1]));
		printf("dest: %s\n", dest);
		free(dest);
		return 0;
	}
...
```

The interesting thing about the test case is that it also contains false positives called "-fake[n]" options:
```
...
	else if (strcmp(argv[1],"-fake1") == 0){
		//fake option to make sure we aren't picking up false positives
		strcpy(dest,"fake");
		free(dest);
		return 0;
	}
	else if (strcmp(argv[1],"-fake2") == 0){
		//fake option to make sure we aren't picking up false positives
		strcpy(dest,"fake");
		free(dest);
		return 0;
	}
	else if (strcmp(argv[1],"-fake3") == 0){
		//fake option to make sure we aren't picking up false positives
		strcpy(dest,"fake");
		free(dest);
		return 0;
	}
...
```
These never get reported since they do not include tainted c functions.


## Further Reading and References
1. Edward J. Schwartz, Thanassis Avgerinos, and David Brumley. 2010. All You Ever Wanted to Know about Dynamic Taint Analysis and Forward Symbolic Execution (but Might Have Been Afraid to Ask). In Proceedings of the 2010 IEEE Symposium on Security and Privacy (SP '10). IEEE Computer Society, USA, 317–331. DOI:https://doi.org/10.1109/SP.2010.26 - https://kilthub.cmu.edu/articles/All_You_Ever_Wanted_to_Know_About_Dynamic_Taint_Analysis_and_Forward_Symbolic_Execution_but_might_have_been_afraid_to_ask_/6468308/files/11896859.pdf 
2. Cha SK, Avgerinos T, Rebert A, Brumley D. Unleashing mayhem on binary code. In2012 IEEE Symposium on Security and Privacy 2012 May 20 (pp. 380-394). IEEE. - https://dl.acm.org/doi/10.1109/SP.2012.31 
3. Wang, F. and Shoshitaishvili, Y., 2017, September. Angr-the next generation of binary analysis. In 2017 IEEE Cybersecurity Development (SecDev) (pp. 8-9). IEEE. - Wang, F. and Shoshitaishvili, Y., 2017, September. Angr-the next generation of binary analysis. In 2017 IEEE Cybersecurity Development (SecDev) (pp. 8-9). IEEE. - https://ieeexplore.ieee.org/abstract/document/8077799 

